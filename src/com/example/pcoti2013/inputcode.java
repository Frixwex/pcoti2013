package com.example.pcoti2013;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;

public class inputcode extends TabActivity
{
        /** Called when the activity is first created. */
        @Override
        public void onCreate(Bundle savedInstanceState)
        {
        	ActivityHelper.initialize(this);
                super.onCreate(savedInstanceState);
                setContentView(R.layout.inputview);

                // create the TabHost that will contain the Tabs
                TabHost tabHost = (TabHost)findViewById(android.R.id.tabhost);


                TabSpec tab1 = tabHost.newTabSpec("Makanan Pokok");
                TabSpec tab2 = tabHost.newTabSpec("Lauk Pauk");
                TabSpec tab3 = tabHost.newTabSpec("Sayuran");
                TabSpec tab4 = tabHost.newTabSpec("Buah-buahan");

               // Set the Tab name and Activity
               // that will be opened when particular Tab will be selected
                tab1.setIndicator("Pokok");
                tab1.setContent(new Intent(this,inppokokcode.class));
               
                tab2.setIndicator("Lauk");
                tab2.setContent(new Intent(this,inplaukcode.class));

                tab3.setIndicator("Sayur");
                tab3.setContent(new Intent(this,inpsayurcode.class));
                
                tab4.setIndicator("Buah");
                tab4.setContent(new Intent(this,inpbuahcode.class));
               
                /** Add the tabs  to the TabHost to display. */
                tabHost.addTab(tab1);
                tabHost.addTab(tab2);
                tabHost.addTab(tab3);
                tabHost.addTab(tab4);

        }

        @Override
        public void onBackPressed() {
	    	Intent tb = new Intent(inputcode.this, todaycode.class);
			startActivity(tb);
			super.finish();
		} 
} 