package com.example.pcoti2013;

import java.util.ArrayList;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

public class foodDBAdapter extends SQLiteAssetHelper
{
	private static final String	DB_NAME		= "allfood";
	private static final int	DB_VER		= 1;
	public static final String	tipe		= "tipe";
	public static final String	TABLE_NAME	= "food";
	public static final String	COL_ID		= "food_id";
	public static final String	COL_NAME	= "nama";
	public static final String	COL_K		= "karboh";
	public static final String	COL_C		= "kalori";
	public static final String	COL_P		= "protein";
	public static final String	COL_L		= "lemak";
	public static final String	COL_vA		= "vitA";
	public static final String	COL_vB		= "vitB";
	public static final String	COL_vC		= "vitC";

	private static final String	TAG			= "foodDBAdapter";
	private static foodDBAdapter dbinstance = null;
	private static SQLiteDatabase db;

	public foodDBAdapter(Context context)
	{
		super(context,DB_NAME,null,DB_VER);
		// TODO Auto-generated constructor stub
	}

	public static foodDBAdapter getInstance(Context context)
	{
		if(dbinstance==null){
			dbinstance = new foodDBAdapter(context);
			db = dbinstance.getWritableDatabase();
		}
		return dbinstance;
	}
	
	 @Override
	 public synchronized void close()
	 {
	    super.close();
	    if (dbinstance != null)
	    {
	    	dbinstance.close();
	    }
	 }
	
	
	public void createFood(food_object food)
	{
		ContentValues val = new ContentValues();
		val.put(COL_NAME, food.getName());
		val.put(COL_C, food.getKalori());
		val.put(COL_K, food.getKarboh());
		val.put(COL_P, food.getProtein());
		val.put(COL_L, food.getLemak());
		val.put(COL_vA, food.getVitA());
		val.put(COL_vB, food.getVitB());
		val.put(COL_vC, food.getVitC());
		val.put(tipe, food.getTipe());
		db.insert(TABLE_NAME, null, val);
	}
	
	public boolean deleteContact(int id)
	{
		return db.delete(TABLE_NAME, COL_ID + "=" + id, null) > 0;
	}
	
	/**
	 * Method untuk mengambil semua data soal
	 */
	public ArrayList<food_object> getAllfood()
	{
		ArrayList<food_object> listFood = new ArrayList<food_object>();

		Cursor cursor = db.query(TABLE_NAME, new String[]
		{
				COL_ID,
				COL_NAME,
				COL_C,
				COL_K,
				COL_P,
				COL_L,
				COL_vA,
				COL_vB,
				COL_vC,
				tipe, 
		}, null, null, null, null, null);

		if (cursor.moveToFirst())
		{
			do
			{
				food_object datamakanan = new food_object();
				datamakanan.setId(cursor.getInt(cursor.getColumnIndexOrThrow(COL_ID)));
				datamakanan.setName(cursor.getString(cursor.getColumnIndexOrThrow(COL_NAME)));
				datamakanan.setKalori(cursor.getDouble(cursor.getColumnIndexOrThrow(COL_C)));
				datamakanan.setKarboh(cursor.getDouble(cursor.getColumnIndexOrThrow(COL_K)));
				datamakanan.setProtein(cursor.getDouble(cursor.getColumnIndexOrThrow(COL_P)));
				datamakanan.setLemak(cursor.getDouble(cursor.getColumnIndexOrThrow(COL_L)));
				datamakanan.setTipe(cursor.getInt(cursor.getColumnIndexOrThrow(tipe)));
				datamakanan.setVitA(cursor.getDouble(cursor.getColumnIndexOrThrow(COL_vA)));
				datamakanan.setVitB(cursor.getDouble(cursor.getColumnIndexOrThrow(COL_vB)));
				datamakanan.setVitC(cursor.getDouble(cursor.getColumnIndexOrThrow(COL_vC)));

				listFood.add(datamakanan);
			} while (cursor.moveToNext());
		}

		return listFood;

	}

}

