package com.example.pcoti2013;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

public class inppokokcode extends Activity{
	
	private foodDBAdapter mDb;
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pokokview);
		
		ActivityHelper.initialize(this);
		mDb = foodDBAdapter.getInstance(this);	
		final ArrayList<food_object> list_food = new ArrayList<food_object>();
		
		list_food.add(new food_object("Nasi", 178, 40.6, 2.1, 0.1, 0, 0.02, 0));
		list_food.add(new food_object("Bihun", 360, 82.1, 4.7, 0.1, 0, 0, 0));
		list_food.add(new food_object("Kentang", 83, 19.1, 2.0, 0.1, 0, 0.11, 17));
		list_food.add(new food_object("Tape", 173, 42.5, 0.5, 0.1, 20, 0.07, 0));
		list_food.add(new food_object("Singkong", 146, 34.7, 1.2, 0.3, 0, 0.06, 30));
		list_food.add(new food_object("Sagu", 353, 84.7, 0.7, 0.25, 0, 0.01, 0));
		list_food.add(new food_object("Ubi Jalar Merah", 323, 27.9, 1.8, 0.7, 7700, 0.09, 22));
		list_food.add(new food_object("Ubi Jalar Putih", 123, 27.9, 1.8, 0.7, 60, 0.09, 22));
		list_food.add(new food_object("Talas", 98, 23.7, 1.9, 0.2, 0, 0.13, 4));
		list_food.add(new food_object("Roti", 248, 50, 8, 1.2, 0, 0.1, 0));
		list_food.add(new food_object("Gaplek", 335, 81.3, 1.5, 0.7, 0, 0.04, 0));
		list_food.add(new food_object("Gembili", 95, 22.4, 1.5, 0.1, 0, 0.05, 4));
		list_food.add(new food_object("Havermout", 390, 86.2, 14.2, 7.4, 0, 0.6, 0));
		
		Collections.sort(list_food, new Comparator<food_object>() {
			@Override
			public int compare(food_object lhs, food_object rhs) {
				// TODO Auto-generated method stub
				return lhs.getName().compareTo(rhs.getName());
			}
		});
		
	    ListView list;
	    final String[] food_name = new String[list_food.size()];
	    for(int i=0;i<list_food.size();i++){
	    	food_name[i]=list_food.get(i).getName();
	    }
	    
	    Integer[] imageId = {
	            R.drawable.image1,
	    };	  
	 
        customlist adapter = new
        customlist(inppokokcode.this, food_name, imageId);
		list=(ListView)findViewById(R.id.listPokok);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
        	
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(inppokokcode.this, food_name[+ position] +" ditambahkan", Toast.LENGTH_SHORT).show();
                mDb.createFood(list_food.get(+ position));
                
                Intent toTab = new Intent(inppokokcode.this,todaycode.class); 
				startActivity(toTab);
				finish();
            }
            
        });
        
    }
	
	@Override
	  public void onBackPressed() {
	    this.getParent().onBackPressed();   
	  }
	
}