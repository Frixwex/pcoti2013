package com.example.pcoti2013;

import android.util.Log;

public class utilscode {
	/**
	 * Method untuk debugging apps
	 * 
	 * @param TAG
	 *            TAG class
	 * @param message
	 *            Pesan
	 */
	public static void TRACE(String TAG, String message)
	{
		if (BuildConfig.DEBUG)
		{
			Log.d(TAG, message);
		}
	}

}

