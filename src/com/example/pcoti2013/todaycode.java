package com.example.pcoti2013;

import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

public class todaycode extends Activity{
		
		private foodDBAdapter mDb;
		private Button bTambah;
		
		@Override
		protected void onCreate(Bundle savedInstanceState){
			super.onCreate(savedInstanceState);
			setContentView(R.layout.todayview);
			
			ActivityHelper.initialize(this);
			mDb = foodDBAdapter.getInstance(this);	
			bTambah = (Button) findViewById(R.id.bInput);
			final ArrayList<food_object> list_food = mDb.getAllfood();

			if(list_food.size()!=0){
				// 	ngambil nama food
				final ListView list;
				final String[] food_name = new String[list_food.size()];
				
			    for(int i=0;i<list_food.size();i++){
			    	food_name[i]=list_food.get(i).getName();}
			    
			    Integer[] imageId = {
			            R.drawable.image1,};
			    
			    final customlist adapter = new customlist(todaycode.this, food_name, imageId);
				list=(ListView)findViewById(R.id.listToday);
				
		        list.setAdapter(adapter);
		        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
		            @Override
		            public void onItemClick(AdapterView<?> parent, View view,int position, long id) {
		            	AlertDialog.Builder alertDialog = new AlertDialog.Builder(todaycode.this);
		                alertDialog.setTitle("Hapus dari daftar");
		                alertDialog.setMessage("Anda yakin akan menghapus "+list_food.get(+ position).getName().toLowerCase() +" dari daftar?");
		         
		                // Setting Icon to Dialog
		                // alertDialog.setIcon(R.drawable.delete);
		                final int del_position=position;
		                alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
		                    @Override
		                    public void onClick(DialogInterface dialog,int which) {
		                         //delete from database     
		                         mDb.deleteContact(list_food.get(+ del_position).getId());
		                         //reload
		                         final ArrayList<food_object> list_food = mDb.getAllfood();
		                         final ListView list;
		                         final String[] food_name = new String[list_food.size()];

		                         for(int i=0;i<list_food.size();i++){
		                             food_name[i]=list_food.get(i).getName();}

		                         Integer[] imageId = {
		                             R.drawable.image1,};

		                         final customlist adapter = new customlist(todaycode.this, food_name, imageId);
		                         list=(ListView)findViewById(R.id.listToday);
		                         list.setAdapter(null);
		                         list.setAdapter(adapter);
		                         }
		                   });
		         
		                alertDialog.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
		                    @Override
							public void onClick(DialogInterface dialog, int which) {
		                    	dialog.cancel();
		                    }
		                });
		         
		                // Showing Alert Message
		                alertDialog.show();
		                
		            }
		        });
			}
	        // button function
	        bTambah.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent toTab = new Intent(todaycode.this,inputcode.class); 
					startActivity(toTab);
					finish();
				}
			});
	}
	   
		@Override  
	    public boolean onKeyDown(int keyCode, KeyEvent event)  
	    {   
		    if(keyCode==KeyEvent.KEYCODE_BACK)  
		    {  
		    	Intent tb = new Intent(todaycode.this, berandacode.class);
				startActivity(tb);
				finish();
				return true;
		    }  
		    return false;
		} 
}