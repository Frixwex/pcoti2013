package com.example.pcoti2013;

public class personalInputcode {
	private int     id;
	private String  name;
	private int  	 weight, height;
	 
	public personalInputcode() {
	// TODO Auto-generated constructor stub
	}
	 	 
	public personalInputcode(String name, int weight, int height)
	{
	     super();
	     this.name = name;
	     this.weight = weight;
	     this.height = height;
	}
	 
	public int getId()
	{
	     return id;
	}
	 
	public void setId(int id)
	{
	     this.id = id;
	}
		 
	public String getName()
	{
	     return name;
	}
	
	public void setName(String name){
	     this.name = name;
	}
	 
	public int getHeight() {
		return height;
	}
	 	 
	public int getWeight() {
		return weight;
	}
	
	public void setHeight(int height) {
		this.height = height;
	}
	
	public void setWeight(int weight) {
		this.weight = weight;
	}
}
