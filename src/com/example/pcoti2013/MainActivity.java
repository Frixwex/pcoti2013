package com.example.pcoti2013;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends Activity {

	Button bReg,bMain;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		bMain= (Button) findViewById(R.id.bMain);
		ActivityHelper.initialize(this);

		
		bMain.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent toMain = new Intent(MainActivity.this, berandacode.class);
				startActivity(toMain);
				finish();
			}
		});
	}
	
	@Override  
    public boolean onKeyDown(int keyCode, KeyEvent event)  
    {  
		if (keyCode == KeyEvent.KEYCODE_BACK){
            finish();
            return true;
        }
		return false;
    }

}
