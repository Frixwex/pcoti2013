package com.example.pcoti2013;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

public class berandacode extends Activity{	
	private Button 	bToInput;
	public int 		kalori = 0;
	public Double 	karboh = 0.0 ,
					protein = 0.0 ,
					lemak = 0.0 ,
					vitA = 0.0 ,
					vitB = 0.0 ,
					vitC = 0.0 ,
					
					sK = 400.0 ,
					sP = 20.0, 
					sL = 0.5 ,
					svA = 0.5,
					svC = 20.0;
	private foodDBAdapter mDb;
	private ProgressBar barK,barP,barL,barvA,barvB,barvC;
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.berandaview);
		
		ActivityHelper.initialize(this);
		bToInput = (Button)findViewById(R.id.bToInput);
		bToInput.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent toTab = new Intent(berandacode.this,todaycode.class); 
				startActivity(toTab);
				finish();
			}
		});
		
			// get scores
		mDb = foodDBAdapter.getInstance(this);
		final ArrayList<food_object> list_food = mDb.getAllfood();
			
		for(int i=0;i<list_food.size();i++){
			kalori += list_food.get(i).getKalori();
			karboh += list_food.get(i).getKarboh();
			protein += list_food.get(i).getProtein();
			lemak += list_food.get(i).getLemak();
			vitA += list_food.get(i).getVitA();
			vitB += list_food.get(i).getVitB();
			vitC += list_food.get(i).getVitC();	
		}
		
		TextView tKaloriN = (TextView) findViewById(R.id.tKaloriN);
		tKaloriN.setText(""+kalori);
		
		barK = (ProgressBar) findViewById(R.id.barK);
		barP = (ProgressBar) findViewById(R.id.barP);
		barL = (ProgressBar) findViewById(R.id.barL);
		barvA = (ProgressBar) findViewById(R.id.barvA);
		barvC = (ProgressBar) findViewById(R.id.barvC);
		
		barK.setMax((int)Math.floor(sK));
		barL.setMax((int)Math.floor(sL*100));
		barP.setMax((int)Math.floor(sP*10));
		barvA.setMax((int)Math.floor(svA*100));
		barvC.setMax((int)Math.floor(svC));
		
		barK.setProgress((int)Math.floor(karboh));
		barP.setProgress((int)Math.floor(protein*10));
		barL.setProgress((int)Math.floor(lemak*100));
		barvA.setProgress((int)Math.floor(vitA*100));
		barvC.setProgress((int)Math.floor(vitC));
	}
	
	@Override  
    public boolean onKeyDown(int keyCode, KeyEvent event)  
    {  
        //replaces the default 'Back' button action  
	    if(keyCode==KeyEvent.KEYCODE_BACK)  
	    {   
	    	Intent bm = new Intent(berandacode.this, MainActivity.class);
			startActivity(bm);
			finish();
			return true;
	    }  
    return false; 
    }
}
