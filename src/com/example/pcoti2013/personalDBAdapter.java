package com.example.pcoti2013;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class personalDBAdapter
{
	private static final String	DB_NAME		= "profile_db";
	private static final int	DB_VER		= 1;

	public static final String	TABLE_NAME	= "profile";
	public static final String	COL_ID		= "_id";
	public static final String	COL_NAME	= "name";
	public static final String	COL_W		= "weight";
	public static final String	COL_H		= "height";

	private static final String	TAG			= "profileDBAdapter";
	private DatabaseHelper		dbHelper;
	private SQLiteDatabase		db;

	private static final String	DB_CREATE	= "create table contact (_id integer primary key, name text not null, weight integer not null,height integer not null);";

	private final Context		context;

	private static class DatabaseHelper extends SQLiteOpenHelper
	{

		public DatabaseHelper(Context context)
		{
			// TODO Auto-generated constructor stub
			super(context, DB_NAME, null, DB_VER);
		}

		@Override
		public void onCreate(SQLiteDatabase db)
		{
			// TODO Auto-generated method stub
			db.execSQL(DB_CREATE);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
		{
			// TODO Auto-generated method stub
			Log.d(TAG, "upgrade DB");
			db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
			onCreate(db);
		}

	}

	public personalDBAdapter(Context context)
	{
		this.context = context;
		// TODO Auto-generated constructor stub
	}

	public personalDBAdapter open() throws SQLException
	{
		dbHelper = new DatabaseHelper(context);
		db = dbHelper.getWritableDatabase();
		return this;
	}

	public void close()
	{
		dbHelper.close();
	}

	public void createContact(personal_object contact)
	{
		ContentValues val = new ContentValues();
		val.put(COL_NAME, contact.getName());
		val.put(COL_W, contact.getWeight());
		val.put(COL_H, contact.getHeight());
		db.insert(TABLE_NAME, null, val);
	}

	public boolean deleteContact(int id)
	{
		return db.delete(TABLE_NAME, COL_ID + "=" + id, null) > 0;
	}

	public Cursor getAllContact()
	{
		return db.query(TABLE_NAME, new String[]
		{
				COL_ID, COL_NAME, COL_W, COL_H
		}, null, null, null, null, null);
	}

	public Cursor getSingleContact(int id)
	{
		Cursor cursor = db.query(TABLE_NAME, new String[]
		{
				COL_ID, COL_NAME, COL_W, COL_H
		}, COL_ID + "=" + id, null, null, null, null);

		if (cursor != null)
			cursor.moveToFirst();

		return cursor;
	}

	public boolean updateContact(personal_object contact)
	{
		ContentValues val = new ContentValues();
		val.put(COL_NAME, contact.getName());
		val.put(COL_W, contact.getWeight());
		val.put(COL_H, contact.getHeight());

		return db.update(TABLE_NAME, val, COL_ID + "=" + contact.getId(), null) > 0;
	}

}

