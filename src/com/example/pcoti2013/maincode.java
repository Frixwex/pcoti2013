package com.example.pcoti2013;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class maincode extends Activity{
	private Button bToInput;
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.mainview);
	
		bToInput = (Button)findViewById(R.id.bToInput);
		bToInput.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//Toast.makeText(maincode.this, "menuju Input Page", Toast.LENGTH_SHORT).show();
				Intent toTab = new Intent(maincode.this,inputcode.class); 
				startActivity(toTab);
			}
		});
	}
	
}
