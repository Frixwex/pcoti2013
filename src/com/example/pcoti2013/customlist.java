package com.example.pcoti2013;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
 
public class customlist extends ArrayAdapter<String>{
 
	private final Activity context;
	private final String[] food_name;
	private final Integer[] imageId;

	
	public customlist(Activity context, String[] food_name, Integer[] imageId) {
			super(context, R.layout.list_single, food_name);
			this.context = context;
			this.food_name = food_name;
			this.imageId = imageId;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		LayoutInflater inflater = context.getLayoutInflater();
		View rowView= inflater.inflate(R.layout.list_single, null, true);
		TextView txtTitle = (TextView) rowView.findViewById(R.id.txt);
		 
		ImageView imageView = (ImageView) rowView.findViewById(R.id.img);
		txtTitle.setText(food_name[position]);
		 
		imageView.setImageResource(imageId[0]);
		return rowView;
	}
}
